package com.rmpader.security.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.rmpader.security.config.property.AppSecurityProperties;
import com.rmpader.security.data.repository.UserAuthenticationRepository;
import com.rmpader.security.util.UserDetailsImpl;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

/**
 * @author RMPader
 */
public class GoogleAuthenticationFilter extends AbstractAuthenticationProcessingFilter {


    private UserAuthenticationRepository userAuthenticationRepository;
    private GoogleIdTokenVerifier verifier;

    public GoogleAuthenticationFilter(AppSecurityProperties properties, UserAuthenticationRepository userAuthenticationRepository) {
        super(new AntPathRequestMatcher(properties.getGoogleLoginUrl(), "POST"));
        this.userAuthenticationRepository = userAuthenticationRepository;
        this.verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
                .setAudience(Arrays.asList(properties.getGoogleClientId()))
                .build();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        try {
            GoogleIdToken idToken = verifier.verify(request.getParameter("token"));

            if (idToken != null && idToken.getPayload()
                                          .getEmail()
                                          .equals(request.getParameter("identity"))) {
                UserDetails details = new UserDetailsImpl(userAuthenticationRepository.findOne(idToken.getPayload()
                                                                                                      .getEmail())); // get from DB
                return new PreAuthenticatedAuthenticationToken(details, idToken.getPayload()
                                                                               .getEmail(), details.getAuthorities());
            } else {
                throw new BadCredentialsException("Identity mismatch for token");
            }
        } catch (GeneralSecurityException e) {
            throw new AuthenticationServiceException("Error while authenticating with Google", e);
        }
    }
}
