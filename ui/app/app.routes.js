(function() {
  'use strict';

  angular
    .module('app')
    .config(stateConfig);

  function stateConfig($stateProvider, $urlRouterProvider){
    //set default path here
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('login', {
        url: "/login",
        templateUrl: "app/login/login.template.html",
        controller: "LoginController as vm"
      })
      .state('profile', {
        url: "/profile",
        templateUrl: "app/profile/profile.template.html",
        controller: "ProfileController as vm"
      });;
  }
})();
