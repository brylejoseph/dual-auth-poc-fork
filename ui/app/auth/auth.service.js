(function() {
  'use strict';

  var app = angular
  .module('app')
  .service('authService', authService)
  .run(function($rootScope, $location, $state, authService) {
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
        var toLogin = toState.name === "login";
        if(toLogin){
          if(authService.isLoggedIn){
            //must not be able to navigate here when already logged in
            e.preventDefault();
          }else{
            return; // no need to redirect
          }
        }else{
          if(!authService.isLoggedIn) {
            e.preventDefault(); // stop current execution
            $state.go('login'); // unauthenticated, go to login
          }
        }
      });
    })
    .run(function(GAuth, $rootScope) {
        var CLIENT = '218178306686-83pqm9g2a7s214hp03kv51lcgm5nniid.apps.googleusercontent.com';

        GAuth.setClient(CLIENT);
    });

  function authService($http, $q, GAuth, GData){
    var service = this;

    service.login = login;
    service.googleLogin = googleLogin;
    service.isLoggedIn = false;
    service.loginType = 'default';
    service.logout = logout;

    var apiURL = 'http://localhost:8080';

    function getCsrfToken(x,str){
      var deferred = $q.defer();
      console.log("requesting csrf token...");
      // Simple GET request example:
      $http({
        method: 'GET',
        url: apiURL + '/me'
      }).then(function() {
        deferred.resolve();
      }, function(error) {
        if(error.status === 404){
          deferred.reject("Error: Unable to connect to the authentication service.");
        }else{
          deferred.reject("Error: CSRF error.");
        }
      });
      return deferred.promise;
    }

    function login(username,password){
      var deferred = $q.defer();
      getCsrfToken()
        .then(function(){
          console.log("Logging in...");
          $http({
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param({username: username, password: password}),
            url: apiURL + '/login'
          }).then(function(success){
            service.isLoggedIn = true;
            service.loginType = 'default';
            deferred.resolve(success.data);
          },function(error){
            if(error.status === 404){
              deferred.reject("Error: Unable to connect to the login service.");
            }else{
              deferred.reject(error.data);
            }
          });
        }, function(error){
          deferred.reject(error);
        });
      return deferred.promise;
    }

    function googleLogin(){
      var deferred = $q.defer();

      GAuth.login()
        .then(function(){
          var googleUser = GData.getUser();
          GAuth.getToken().then(function(googleToken){
            getCsrfToken().then(function(){
                var authToken = googleToken.id_token;
                console.log('authToken', authToken);
                console.log('Email',googleUser.email);
                $http({
                  method: 'POST',
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data: $.param({identity: googleUser.email, token: authToken}),
                  url: apiURL + '/oauth/google'
                }).then(function(success){
                  service.isLoggedIn = true;
                  service.loginType = 'google';
                  deferred.resolve(success.data);
                },function(error){
                  if(error.status === 404){
                    deferred.reject("Error: Unable to connect to login server.");
                  }else{
                    deferred.reject(error.data);
                  }
                });
              }, function(error){
                deferred.reject(error);
              });
          },function(error){
            deferred.reject(error.data);
          });
      }, function() {
          deferred.reject('Google authentication failed');
      });
      return deferred.promise;
    }

    function logout(){
      if(service.loginType === 'google'){
        return GAuth.logout().then(function(success){
          console.log("Logged out of google");
          service.isLoggedIn = false;
          service.loginType = 'default';
        },function(error){
          console.log(error);
        });
      }else{
        service.isLoggedIn = false;
        service.loginType = 'default';
      }
    }
  }

})();
